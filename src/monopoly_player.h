#ifndef MONOPOLY_PLAYER
#define MONOPOLY_PLAYER

#define LINE_BUFFER_LENGTH 81

void players_init( uint8_t player_count );

PLAYER_T* player_get( uint8_t index );

PLAYER_T* player_bank_get( void );

void player_money_transfer( PLAYER_T* from, PLAYER_T* to, int32_t amount );

bool player_query( const wchar_t* accept );

wchar_t* player_line_query( wchar_t* description );

long int player_numeric_query( wchar_t* description );

void player_exit_cleanup( void );

#endif /* #ifndef MONOPOLY_PLAYER */
