#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <malloc.h>
#include <ctype.h>
#include <wchar.h>

#include "monopoly_common.h"
#include "monopoly_table.h"

#include "monopoly_player.h"

static PLAYER_T banker;
static PLAYER_T players[PLAYER_COUNT_MAX];

static TOKEN_T tokens[] =
{
{ TOKEN_TERRIER, L"Terrieri", NULL },
{ TOKEN_BATTLESHIP, L"Taistelulaiva", NULL },
{ TOKEN_AUTOMOBILE, L"Auto", NULL },
{ TOKEN_TOP_HAT, L"Silinterihattu", NULL },
{ TOKEN_THIMBLE, L"Sormustin", NULL },
{ TOKEN_SHOE, L"Kenkä", NULL },
{ TOKEN_WHEELBARROW, L"Kottikärryt", NULL },
{ TOKEN_IRON, L"Silitysrauta", NULL },
{ TOKEN_TRAIN, L"Juna", NULL },
{ TOKEN_CANNON, L"Tykki", NULL }
};

const uint8_t tokens_total_count = sizeof( tokens ) / sizeof( tokens[0] );

TOKEN_T* player_token_select( PLAYER_T* player )
    {
    uint8_t i;
    uint8_t token_index = 1;
    long int token_select;
    TOKEN_T* token = NULL;

    wprintf(L"Vapaat pelimerkit:\n");

    for ( i = 0; i < tokens_total_count; i++ )
        {
        if ( tokens[i].owner == NULL )
            {
            wprintf(L"\t%i. %ls\n", token_index, tokens[i].name);
            token_index++;
            }
        }

    token_select = player_numeric_query( L"Valitse pelimerkkisi: " );

    if ( ( token_select == 0 ) || ( token_select >= token_index ) )
        {
        wprintf(L"\to_O wut?\n");
        token_select = 1;
        }

    token_index = 1;

    for ( i = 0; i < tokens_total_count; i++ )
        {
        if ( tokens[i].owner == NULL )
            {
            if ( token_index == token_select )
                {
                tokens[i].owner = player;
                token = &( tokens[i] );
                break;
                }
            token_index++;
            }
        }

    assert( token != NULL );

    wprintf(L"Valitsit pelimerkiksesi: %ls\n", token->name );
    return token;
    }

void player_banker_init( void )
    {
    wchar_t name[] = L"Pankki";

    banker.token = NULL;
    banker.name = calloc((wcslen(name) + 1) * sizeof(wchar_t), sizeof(wchar_t));
    wcsncpy (banker.name, name, wcslen(name));
    banker.account_balance = BANK_MONEY_AMOUNT;
    banker.current_place = table_square_get( 0 );
    }

void players_init( uint8_t player_count )
    {
    uint8_t i;

    PLAYER_T* bank = &( banker );
    PLAYER_T* player = NULL;

    player_banker_init();

    for ( i = 0; i < PLAYER_COUNT_MAX; i++ )
        {
        player = &( players[i] );

        player->index = i;
        player->token = NULL;
        player->name = NULL;
        player->account_balance = 0;
        player->current_place = table_square_get( 0 );

        if ( i < player_count )
            {
            wprintf(L"********************************************************************************\n");

            while ( player->name == NULL )
                {
                wprintf(L"Pelaajan %i nimi: ", i+1 );
                player->name = player_line_query( NULL );
                }

            player->token = player_token_select( player );
            player_money_transfer( bank, player, PLAYER_MONEY_START );
            }
        }
    }

PLAYER_T* player_get( uint8_t index )
    {
    assert( index < PLAYER_COUNT_MAX );

    return &players[index];
    }

PLAYER_T* player_bank_get( void )
    {
    return &banker;
    }

void player_money_transfer( PLAYER_T* from, PLAYER_T* to, int32_t amount )
    {
    wprintf(L"%i eur, %ls -> %ls\n", amount, from->name, to->name );
    from->account_balance -= amount;
    to->account_balance += amount;
    }

bool player_query( const wchar_t* accept )
    {
    wchar_t* answer;
    size_t answer_length;
    size_t character;
    bool value;

    answer = player_line_query( NULL );

    if ( answer == NULL )
        {
        return false;
        }

    answer_length = wcslen( answer );

    for ( character = 0; character < answer_length; character++ )
        {
        /* Convert player's answer to lowercase */
        answer[character] = tolower( answer[character] );
        }

    if ( wcsstr( answer, accept ) != NULL )
        {
        value = true;
        }
    else
        {
        value = false;
        }

    free( answer );

    return value;
    }

wchar_t *player_line_query(wchar_t *description )
    {
    wchar_t* line = NULL;

    if ( description != NULL )
        {
        wprintf(L"%ls", description );
        }

    line = calloc(sizeof(wchar_t) * (LINE_BUFFER_LENGTH + 1), sizeof(wchar_t));
    assert(line);
    line = fgetws(line, LINE_BUFFER_LENGTH, stdin);
    assert(line);

    if (line != NULL)
        {
        /* Clean line-endings ( LF, CR, CRLF, LFCR ) */
        line[wcscspn(line, L"\r\n")] = 0;

        if ( wcslen( line ) == 0 )
            {
            /* Player just pressed enter */
            free( line );
            line = NULL;
            }
        }

    return line;
    }

long int player_numeric_query(wchar_t *description )
    {
    long int answer_int;
    wchar_t* answer = NULL;

    while( answer == NULL )
        {
        answer = player_line_query( description );
        }

    answer_int = wcstol( answer, NULL, 10 );
    free( answer );

    return answer_int;
    }

void player_exit_cleanup( void )
    {
    uint8_t i;

    free( banker.name );

    for ( i = 0; i < PLAYER_COUNT_MAX; i++ )
        {
        if ( players[i].name != NULL )
            {
            free( players[i].name );
            }
        }
    }

